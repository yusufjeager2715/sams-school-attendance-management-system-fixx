package com.example.hp.absensi;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.hp.absensi.DataJadwal;
import com.example.hp.absensi.R;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomAdapterJadwal extends BaseAdapter {

    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater = null;

    public CustomAdapterJadwal(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data = d;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.activity_rowjadwal, null);
        TextView id_jadwal = (TextView) vi.findViewById(R.id.txtid_jadwal);
        TextView matakuliah = (TextView) vi.findViewById(R.id.txt_matakuliah);
        TextView kelas = (TextView) vi.findViewById(R.id.txt_kelas);
        TextView dosen = (TextView) vi.findViewById(R.id.txt_dosen);
        TextView hari = (TextView) vi.findViewById(R.id.txt_hari);
        TextView jamawal = (TextView) vi.findViewById(R.id.txt_jamawal);
        TextView jamakhir = (TextView) vi.findViewById(R.id.txt_jamakhir);
        HashMap<String, String> berita = new HashMap<String, String>();
        berita = data.get(position);

        id_jadwal.setText(berita.get(DataJadwal.TAG_IDJADWAL));
        matakuliah.setText(berita.get(DataJadwal.TAG_MATAKULIAH));
        kelas.setText(berita.get(DataJadwal.TAG_RUANG));
        dosen.setText(berita.get(DataJadwal.TAG_DOSEN));
        hari.setText(berita.get(DataJadwal.TAG_HARI));
        jamawal.setText(berita.get(DataJadwal.TAG_JAMAWAL));
        jamakhir.setText(berita.get(DataJadwal.TAG_JAMAKHIR));
        return vi;
    }
}
