package com.example.hp.absensi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataJadwal extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    SwipeRefreshLayout swipe;

    String cnim, cnama_mahasiswa, ckelas;
    ListView ls_tampil;
    ArrayList<HashMap<String, String>> daftar_jadwal = new ArrayList<HashMap<String, String>>();
    String url_tampil = URL_SERVER.DAFTARJADWAL;
    static final String TAG_IDJADWAL= "id_jadwal";
    static final String TAG_RUANG = "nama_ruang";
    static final String TAG_MATAKULIAH= "nama_matkul";
    static final String TAG_DOSEN = "nama_dosen";
    static final String TAG_HARI = "hari";
    static final String TAG_JAMAWAL = "jam_awal";
    static final String TAG_JAMAKHIR = "jam_akhir";
//    ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    String _id;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datajadwal);
        bacaPreferensi();
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refreshjadwal);
        ls_tampil = (ListView) findViewById(R.id.listjadwal);
        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
//                           daftar_jadwal.clear();
                           new cari_data().execute();

                       }
                   }
        );

    }

    @Override
    public void onRefresh() {

        new cari_data().execute();
    }

    class cari_data extends AsyncTask<String, String, String> {

        String id_jadwal, ruang,matakuliah, dosen, jamawal,jamakhir,hari;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipe.setRefreshing(false);
            daftar_jadwal.clear();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                List<NameValuePair> nvp = new ArrayList<NameValuePair>();
                nvp.add(new BasicNameValuePair("nim", cnim.toString()));
                JSONObject jsonObject = jsonParser.makeHttpRequest(url_tampil, "GET", nvp);
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("djadwal");
                    Log.d("WMWMWMWMWMWMWMWMW : ", DataJadwal.this.toString().length() + "");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        id_jadwal = object.getString(TAG_IDJADWAL);
                        matakuliah = object.getString(TAG_MATAKULIAH);
                        ruang= object.getString(TAG_RUANG);
                        dosen = object.getString(TAG_DOSEN);
                        hari = object.getString(TAG_HARI);
                        jamawal = object.getString(TAG_JAMAWAL);
                        jamakhir = object.getString(TAG_JAMAKHIR);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_IDJADWAL, id_jadwal);
                        map.put(TAG_MATAKULIAH, matakuliah);
                        map.put(TAG_RUANG, ruang);
                        map.put(TAG_DOSEN, dosen);
                        map.put(TAG_HARI, hari);
                        map.put(TAG_JAMAWAL, jamawal);
                        map.put(TAG_JAMAKHIR, jamakhir);
                        daftar_jadwal.add(map);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                swipe.setRefreshing(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            swipe.setRefreshing(false);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    CustomAdapterJadwal adapter ;
                    adapter = new CustomAdapterJadwal(DataJadwal.this, daftar_jadwal);
                    ls_tampil.setAdapter(adapter);
                    final ListView listView = ls_tampil;
                    listView.setTextFilterEnabled(true);
                }
            });
        }
    }


    private void bacaPreferensi() {
        SharedPreferences pref = getSharedPreferences("akun", MODE_PRIVATE);
        cnim = pref.getString("nim", "0");
        cnama_mahasiswa = pref.getString("nama_mahasiswa", "0");
        ckelas = pref.getString("kelas", "0");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(DataJadwal.this,MenuUtama.class));
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

}
