package com.example.hp.absensi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class MenuUtama extends AppCompatActivity {
    TextView tnama,tsmt,tnim;
    String cnim, cnama_mahasiswa, cid_semester,cnama_semester,cfoto_mahasiswa;
    CardView btnutamaabsensi,btnlogout, btnutamajadwal, btnutamahistori,btnutamaabsensiizin;
    CircleImageView fotoimageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuutama);
        bacaPreferensi();
        tnim=findViewById(R.id.menuutamaTnim);
        tnama=findViewById(R.id.menuutamaTnama);
        tsmt=findViewById(R.id.menuutamaTkelas);
        fotoimageView=findViewById(R.id.cobaprofile);

        Picasso.with(getApplicationContext()).load(URL_SERVER.TAMPILGAMBARAKUN+cfoto_mahasiswa).error(R.drawable.iconfinder_graduation_square_academic_cap_school_2824450).into(fotoimageView);
        tnim.setText(" : "+cnim);
        tnama.setText(" : "+cnama_mahasiswa);
        tsmt.setText(" : "+cnama_semester);

        btnutamaabsensi=findViewById(R.id.menubtnabsensi);
        btnutamajadwal=findViewById(R.id.menubtnjadwal);
        btnutamahistori=findViewById(R.id.menubtnhistori);
        btnutamaabsensiizin=findViewById(R.id.menubtnizinatausakit);

        btnlogout=findViewById(R.id.menubtnkeluar);

        btnutamaabsensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuUtama.this,MulaiAbsen.class));
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        btnutamaabsensiizin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuUtama.this,Mulaiizin.class));
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        btnutamajadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuUtama.this,DataJadwal.class));
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        btnutamahistori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuUtama.this,DataHistori.class));
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent keluar = new Intent(MenuUtama.this, LoginActivity.class);
                startActivity(keluar);
                SharedPreferences pref = getSharedPreferences("akun",MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("nim", "0");
                editor.commit();
                finish();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menuubahpassword, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settingspassword) {
            startActivity(new Intent(MenuUtama.this,UbahPassword.class));
            finish();
            overridePendingTransition(R.anim.slide_down, R.anim.slide_bottom);
            return true;

        }

        return super.onOptionsItemSelected(item);
    }
    private void bacaPreferensi() {
        SharedPreferences pref = getSharedPreferences("akun", MODE_PRIVATE);
        cnim = pref.getString("nim", "0");
        cnama_mahasiswa = pref.getString("nama_mahasiswa", "0");
        cid_semester = pref.getString("id_semester", "0");
        cnama_semester = pref.getString("nama_semester", "0");
        cfoto_mahasiswa= pref.getString("foto", "0");
    }
}
