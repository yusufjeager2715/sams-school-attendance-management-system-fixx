package com.example.hp.absensi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class UbahPassword extends AppCompatActivity {
    EditText txtnim,txtpasswordbaru,txtulangipassword;
    String cnim, cnama_mahasiswa, cid_semester,cnama_semester;
    private ProgressDialog progressDialog;
    String status = "";
    String passwordbaru, ulangipasswordbaru;
    Button bEditAkun;
    SwipeRefreshLayout swipeLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubahpassword);
        bacaPreferensi();

        txtnim = (EditText) findViewById(R.id.txt_nimakun);
        txtpasswordbaru = (EditText) findViewById(R.id.txt_passwordbaruakun);
        txtulangipassword = (EditText) findViewById(R.id.txt_ulangipasswordakun);

        bEditAkun = (Button) findViewById(R.id.buttonUbahpassword);
        swipeLayout = findViewById(R.id.swipubahpasswor);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeLayout.setRefreshing(false);
                    }
                }, 5000);
            }
        });
        swipeLayout.setColorScheme(new int[]{android.R.color.holo_blue_bright, android.R.color.holo_green_light, android.R.color.holo_orange_light, android.R.color.holo_red_light});


        txtnim.setText(cnim);
//        txtnama.setText(cnamapenduduk);
//        txtalamat.setText(calamat);
//        txttgllahir.setText(ctgl);
//        txtnope.setText(cnohp);
//        txtpekerjaan.setText(cpekerjaan);
        bEditAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordbaru = txtpasswordbaru.getText().toString();
                ulangipasswordbaru = txtulangipassword.getText().toString();
                if (passwordbaru.equals("")){
                    txtpasswordbaru.setError("Belum diisi");
                    txtpasswordbaru.requestFocus();
                }else if (ulangipasswordbaru.equals("")){
                    txtulangipassword.setError("Belum diisi");
                    txtulangipassword.requestFocus();
                }else{
                    new prosesEdit().execute();
                }
            }
        });
    }

    public class prosesEdit extends AsyncTask<String, String, Void> {
        InputStream content = null;
        String result = "";


        protected void onPreExecute() {
//            progressDialog = new ProgressDialog(UbahPassword.this);
//            progressDialog.setMessage("Loading..");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
            swipeLayout.setRefreshing(true);
        }

        @Override
        protected Void doInBackground(String... params) {
            String url_login = URL_SERVER.UBAHAKUNMAHASISWA;
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("nim", cnim
                    .toString()));
            param.add(new BasicNameValuePair("passwordbaru", txtpasswordbaru.getText()
                    .toString()));
            param.add(new BasicNameValuePair("ulangipassword", txtulangipassword.getText()
                    .toString()));
            StringBuilder str = new StringBuilder();
            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_login);
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));
                HttpResponse response = client.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    content = entity.getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        str.append(line);
                    }
                } else {
                    Log.e("Log", "Failed to download result..");
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            result = str.toString();
            return null;
        }

        public void onPostExecute(Void v) {
            try {
                JSONObject js = new JSONObject(result);
                status = js.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    swipeLayout.setRefreshing(false);
                }
            }, 5000);
            if (status.equals("berhasil")) {
                Toasty.info(getApplicationContext(), "edit Berhasil",
                        Toasty.LENGTH_SHORT).show();
//                Intent keluar = new Intent(UbahPassword.this, LoginActivity.class);
//                startActivity(keluar);
//                SharedPreferences pref = getSharedPreferences("akun", MODE_PRIVATE);
//                SharedPreferences.Editor editor = pref.edit();
//                editor.putString("nis", "0");
//                editor.commit();
//                finish();
            } else {
                Toast.makeText(getApplicationContext(), "edit gagal",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(UbahPassword.this,MenuUtama.class));
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
    private void bacaPreferensi() {
        SharedPreferences pref = getSharedPreferences("akun", MODE_PRIVATE);
        cnim = pref.getString("nim", "0");
        cnama_mahasiswa = pref.getString("nama_mahasiswa", "0");
        cid_semester = pref.getString("id_semester", "0");
        cnama_semester = pref.getString("nama_semester", "0");
    }
}
