package com.example.hp.absensi.model;

/**
 * Created by KUNCORO on 09/08/2017.
 */

public class DataModelHistori {
    private String id_absen, id_jadwal ,file, waktu,status,ket;

    public DataModelHistori() {
    }

    public DataModelHistori(String id_absen, String id_jadwal, String file, String waktu, String status, String ket) {
        this.id_absen = id_absen;
        this.id_jadwal = id_jadwal;
        this.file = file;
        this.waktu = waktu;
        this.status = status;
        this.ket = ket;
    }

    public String getId_absen() {
        return id_absen;
    }

    public void setId_absen(String id_absen) {
        this.id_absen = id_absen;
    }

    public String getId_jadwal() {
        return id_jadwal;
    }

    public void setId_jadwal(String id_jadwal) {
        this.id_jadwal = id_jadwal;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }
}
