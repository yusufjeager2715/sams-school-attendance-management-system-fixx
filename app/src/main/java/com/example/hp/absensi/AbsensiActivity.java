package com.example.hp.absensi;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AbsensiActivity extends AppCompatActivity {
    private int waktu_loading=4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absensi);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //setelah loading maka akan langsung berpindah ke home activity
                Intent home=new Intent(AbsensiActivity.this, LoginActivity.class);
                startActivity(home);
                finish();
                overridePendingTransition(R.anim.slide_down, R.anim.slide_down);
            }
        },waktu_loading);
    }
}
