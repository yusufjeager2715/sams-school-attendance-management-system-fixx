package com.example.hp.absensi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText euser;
    EditText epass;
    Button blogin;
    SwipeRefreshLayout swipeLayout;

    String username, password;
    String cnim, cnama_mahasiswa, cid_semester,cnama_semester,cfoto_mahasiswa;
    int success;
    JSONArray _JSONarray = null;
    private static final String TAG_SUCCESS = "success";
    JSONParser jsonParser = new JSONParser();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        euser = (EditText) findViewById(R.id.txt_Lusername);
        epass = (EditText) findViewById(R.id.txt_Lpassword);
        blogin = (Button) findViewById(R.id.btn_Llogin);
        swipeLayout = findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeLayout.setRefreshing(false);
                    }
                }, 5000);
            }
        });
        swipeLayout.setColorScheme(new int[]{android.R.color.holo_blue_bright, android.R.color.holo_green_light, android.R.color.holo_orange_light, android.R.color.holo_red_light});

        bacaPreferensi();
        blogin.setOnClickListener(this);
//        btn_buatakun.setOnClickListener(this);
        if (cnim.equals("0")) {

        } else {
            startActivity(new Intent(getApplicationContext(), MenuUtama.class));
            finish();
        }


    }


    class login extends AsyncTask<String, String, String> {
        protected void onPreExecute() {
            super.onPreExecute();
            swipeLayout.setRefreshing(true);
        }

        @Override
        protected String doInBackground(String... arg0) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", password));
            System.out.println(params);
            JSONObject json = jsonParser.makeHttpRequest(URL_SERVER.LOGIN, "GET", params);
            if (json != null) {
                try {
                    _JSONarray = json.getJSONArray("Hasil");
                    success = json.getInt(TAG_SUCCESS);
                    if (_JSONarray != null) {
                        if (success == 1) {
                            runOnUiThread(new Runnable() {

                                public void run() {
                                    // TODO Auto-generated method stub
                                    try {
                                        JSONObject object = _JSONarray.getJSONObject(0);

                                        String nim = object.getString("nim");
                                        String nama_mahasiswa = object.getString("nama_mahasiswa");
                                        String id_semester = object.getString("id_semester");
                                        String nama_semester = object.getString("nama_semester");
                                        String foto = object.getString("foto");

                                        startActivity(new Intent(getApplicationContext(), MenuUtama.class));
                                        SharedPreferences pref = getSharedPreferences("akun", MODE_PRIVATE);
                                        SharedPreferences.Editor editor = pref.edit();
                                        editor.putString("nim", nim.toString());
                                        editor.putString("nama_mahasiswa", nama_mahasiswa.toString());
                                        editor.putString("id_semester", id_semester.toString());
                                        editor.putString("nama_semester", nama_semester.toString());
                                        editor.putString("foto", foto.toString());
                                        editor.commit();
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            System.out.println("Data Telah Ditampilkan");
                        } else {
                            // gagal mengedit data
                            System.out.println("Data Gagal Ditampilkan");
                        }
                    } else {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {
            super.onProgressUpdate(progress);
        }

        protected void onPostExecute(String file_url) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    swipeLayout.setRefreshing(false);
                }
            }, 5000);
            if (success == 1) {
                Toasty.info(LoginActivity.this, "Berhasil Login", Toast.LENGTH_SHORT).show();
            } else {
                Toasty.error(LoginActivity.this, "Gagal Login silahkan daftar akun", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Llogin:
                username = euser.getText().toString();
                password = epass.getText().toString();
                if (username.equals("")) {
                    euser.setError("Belum diisi");
                    euser.requestFocus();
                } else if (password.equals("")) {
                    epass.setError("Belum diisi");
                    epass.requestFocus();
                } else {
                    new login().execute();
                }
                break;
//            case R.id.btn_Lbuatakun:
//                startActivity(new Intent(getApplicationContext(),BuatAkun.class));
//                break;
        }
    }

    private void bacaPreferensi() {
        SharedPreferences pref = getSharedPreferences("akun", MODE_PRIVATE);
        cnim = pref.getString("nim", "0");
        cnama_mahasiswa = pref.getString("nama_mahasiswa", "0");
        cid_semester = pref.getString("id_semester", "0");
        cnama_semester = pref.getString("nama_semester", "0");
        cfoto_mahasiswa= pref.getString("foto", "0");
    }
}
