package com.example.hp.absensi;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.example.hp.absensi.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;

public class Mulaiizin extends AppCompatActivity implements  SwipeRefreshLayout.OnRefreshListener {

    ImageView ShowSelectedImage;
    private int GALLERY = 1, CAMERA = 2;
    Bitmap FixBitmap;
    Button UploadImageOnServerButton;
    TextView namamahasiswa,kelas,id_jadwal,viewjadwal;


    String ImageName = "image_data";
    String cnim, cnama_mahasiswa, cid_semester,cnama_semester;
    ProgressDialog progressDialog;
    ByteArrayOutputStream byteArrayOutputStream;
    byte[] byteArray;
    String ConvertImage;
    HttpURLConnection httpURLConnection;
    URL url;
    OutputStream outputStream;
    BufferedWriter bufferedWriter;
    int RC;
    BufferedReader bufferedReader;
    StringBuilder stringBuilder;
    boolean check = true;

    //   jam
    SwipeRefreshLayout swipe;
    public static final String TAG_RESULTS = "results";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_VALUE = "value";
    String tag_json_obj = "json_obj_req";
    private static final String TAG = MulaiAbsen.class.getSimpleName();
    String sidjadwal,sed_ket;
    Spinner spinnerjenis;
    List<String> jenisabsensi =new ArrayList<>();
    EditText ed_ket;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izin);
        bacaPreferensi();




        id_jadwal = (TextView) findViewById(R.id.txtid_jadwalizin);
        viewjadwal = (TextView) findViewById(R.id.txtviewjadwalizin);
        ed_ket = (EditText) findViewById(R.id.txt_kettransaksitambah);
        spinnerjenis = (Spinner) findViewById(R.id.spinnerjenis);
        jenisabsensi.add("Pilih");
        jenisabsensi.add("Izin");
        jenisabsensi.add("Sakit");
        jenisabsensi.add("Telat");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,jenisabsensi);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerjenis.setAdapter(dataAdapter);

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refreshjamizin);
        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           callJam();
                       }
                   }
        );




//
        UploadImageOnServerButton = (Button) findViewById(R.id.buttonUploadabsenizin);
        ShowSelectedImage = (ImageView) findViewById(R.id.imageView3izin);
        if (ContextCompat.checkSelfPermission(Mulaiizin.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                        5);
            }
        }
        byteArrayOutputStream = new ByteArrayOutputStream();

        UploadImageOnServerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sidjadwal =id_jadwal.getText().toString();
                sed_ket =ed_ket.getText().toString();
                if (FixBitmap==null){
                    Toasty.normal(Mulaiizin.this, "Gambar Kosong!", Toasty.LENGTH_SHORT).show();
                }else if(sidjadwal.equals("")) {
                    Toasty.normal(Mulaiizin.this, "Andat belum ada jadwal!", Toasty.LENGTH_SHORT).show();
                }else if(sed_ket.equals("")) {
                    Toasty.normal(Mulaiizin.this, "Kosong Ket", Toasty.LENGTH_SHORT).show();
                }else if(spinnerjenis.getSelectedItem().toString().equals("Pilih")) {
                    Toasty.normal(Mulaiizin.this, "Kosong Sakit Izin Telat", Toasty.LENGTH_SHORT).show();
                }else{
                    UploadImageToServer();
                    FixBitmap=null;
                    ShowSelectedImage.setImageBitmap(null);
                    id_jadwal.setText("");
                    ed_ket.setText("");
                    callJam();
//                    Intent keluar = new Intent(Mulaiizin.this, MenuUtama.class);
//                    startActivity(keluar);
//                    finish();
                }

            }
        });
    }

    //
    public void UploadImageToServer() {
        if (FixBitmap!=null) {
            FixBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);

            byteArray = byteArrayOutputStream.toByteArray();

            ConvertImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
        }else {
            ConvertImage="";
        }
        class AsyncTaskUploadClass extends AsyncTask<Void, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(Mulaiizin.this, "Image is Uploading", "Please Wait", false, false);
            }

            @Override
            protected void onPostExecute(String string1) {
                super.onPostExecute(string1);
                progressDialog.dismiss();
                Toasty.info(Mulaiizin.this, string1, Toasty.LENGTH_LONG).show();


            }

            @Override
            protected String doInBackground(Void... params) {
                ImageProcessClass imageProcessClass = new ImageProcessClass();
                HashMap<String, String> HashMapParams = new HashMap<String, String>();
                HashMapParams.put("nim", cnim);
                HashMapParams.put("id_jadwal", sidjadwal.toString());
                HashMapParams.put("ket", sed_ket.toString());
                HashMapParams.put("status", spinnerjenis.getSelectedItem().toString());
                HashMapParams.put(ImageName, ConvertImage);
                String FinalData = imageProcessClass.ImageHttpRequest(URL_SERVER.TAMBAHIZIN, HashMapParams);
                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();
        AsyncTaskUploadClassOBJ.execute();
    }

    @Override
    public void onRefresh() {
        callJam();
    }

    public class ImageProcessClass {
        public String ImageHttpRequest(String requestURL, HashMap<String, String> PData) {
            StringBuilder stringBuilder = new StringBuilder();
            try {
                url = new URL(requestURL);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setReadTimeout(20000);
                httpURLConnection.setConnectTimeout(20000);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                outputStream = httpURLConnection.getOutputStream();
                bufferedWriter = new BufferedWriter(
                        new OutputStreamWriter(outputStream, "UTF-8"));
                bufferedWriter.write(bufferedWriterDataFN(PData));
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                RC = httpURLConnection.getResponseCode();
                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    stringBuilder = new StringBuilder();
                    String RC2;
                    while ((RC2 = bufferedReader.readLine()) != null) {
                        stringBuilder.append(RC2);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {
            stringBuilder = new StringBuilder();
            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {
                if (check)
                    check = false;
                else
                    stringBuilder.append("&");
                stringBuilder.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));
                stringBuilder.append("=");
                stringBuilder.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }
            return stringBuilder.toString();
        }
    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Photo Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    FixBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    ShowSelectedImage.setImageBitmap(FixBitmap);
                    UploadImageOnServerButton.setVisibility(View.VISIBLE);


                } catch (IOException e) {
                    e.printStackTrace();
                    Toasty.error(Mulaiizin.this, "Failed!", Toasty.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            FixBitmap = (Bitmap) data.getExtras().get("data");
            ShowSelectedImage.setImageBitmap(FixBitmap);
            UploadImageOnServerButton.setVisibility(View.VISIBLE);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cameraizin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuambilcamera_absenizin) {
            showPictureDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private void bacaPreferensi() {
        SharedPreferences pref = getSharedPreferences("akun", MODE_PRIVATE);
        cnim = pref.getString("nim", "0");
        cnama_mahasiswa = pref.getString("nama_mahasiswa", "0");
        cid_semester = pref.getString("id_semester", "0");
        cnama_semester = pref.getString("nama_semester", "0");
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Mulaiizin.this,MenuUtama.class));
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
    private void callJam() {
        swipe.setRefreshing(true);
        // Creating volley request obj
        StringRequest jArr = new StringRequest(Request.Method.POST, URL_SERVER.DATAJAM, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);

                    int value = jObj.getInt(TAG_VALUE);

                    if (value == 1) {

                        String getObject = jObj.getString(TAG_RESULTS);
                        JSONArray jsonArray = new JSONArray(getObject);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);

                            String sid_jadwal = obj.getString("id_jadwal");
                            String snama_matkul = obj.getString("nama_matkul");
                            String snama_dosen = obj.getString("nama_dosen");
                            String snama_ruang = obj.getString("nama_ruang");
                            String shari = obj.getString("hari");
                            String sjamawal = obj.getString("jam_awal");
                            String sjamakhir = obj.getString("jam_akhir");
                            id_jadwal.setText(sid_jadwal);
                            viewjadwal.setText("MataPelajaran: "+snama_matkul+". Guru: "+snama_dosen+". Ruang: "+snama_ruang+". Hari: "+shari+". Jam Awal: "+sjamawal+". Jam Akhir: "+sjamakhir);
                        }

                    } else {
                        id_jadwal.setText("");
                        viewjadwal.setText("Jadwal Belum Ada");
                        Toast.makeText(getApplicationContext(), "Jadwal Belum Ada", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(TAG, "fError: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi Error", Toast.LENGTH_SHORT).show();
                swipe.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("nim", cnim);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jArr, tag_json_obj);
    }


}
