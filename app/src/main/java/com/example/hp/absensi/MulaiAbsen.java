package com.example.hp.absensi;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.example.hp.absensi.app.AppController;
import com.example.hp.absensi.model.DataModelHistori;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import android.location.Location;

import android.location.Criteria;
import android.location.LocationListener;
import android.location.LocationManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;

public class MulaiAbsen extends AppCompatActivity implements LocationListener, SwipeRefreshLayout.OnRefreshListener {

    ImageView ShowSelectedImage;
    private int GALLERY = 1, CAMERA = 2;
    Bitmap FixBitmap;
    Button UploadImageOnServerButtonmasuk,UploadImageOnServerButtonpulang,ceklokasi;
    TextView namamahasiswa,kelas,id_jadwal,viewjadwal,txtketabsensi;


    String ImageName = "image_data";
    String cnim, cnama_mahasiswa, cid_semester,cnama_semester;
    ProgressDialog progressDialog;
    ByteArrayOutputStream byteArrayOutputStream;
    byte[] byteArray;
    String ConvertImage;
    HttpURLConnection httpURLConnection;
    URL url;
    OutputStream outputStream;
    BufferedWriter bufferedWriter;
    int RC;
    BufferedReader bufferedReader;
    StringBuilder stringBuilder;
    boolean check = true;

    //    mylokasi
    private GoogleMap myMap;
    private ProgressDialog myProgress;

    TextView latitude, longitude,jarak;

    double latitudekampus,longitudekampus;
    double latitudesaatini,longitudesaatini;

    private static final String MYTAG = "MYTAG";
    public static final int REQUEST_ID_ACCESS_COURSE_FINE_LOCATION = 100;

    //   jam
    SwipeRefreshLayout swipe;
    public static final String TAG_RESULTS = "results";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_VALUE = "value";
    String tag_json_obj = "json_obj_req";
    private static final String TAG = MulaiAbsen.class.getSimpleName();
    String sidjadwal;
    String sket;
    Double distance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mulaiabsen);
        bacaPreferensi();
        ambillokasi();


        id_jadwal = (TextView) findViewById(R.id.txtid_jadwal);
        viewjadwal = (TextView) findViewById(R.id.txtviewjadwal);
        txtketabsensi = (TextView) findViewById(R.id.txt_ketabsensi);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refreshjamabsn);
        swipe.setOnRefreshListener(this);
        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           callJam();
                           callButtonAktif();
                       }
                   }
        );



        latitudekampus=-1.2177257293292152;
        longitudekampus=116.8529167393684;

        latitude = (TextView) findViewById(R.id.textlangitude);
        longitude = (TextView) findViewById(R.id.textlongitude);
        jarak = (TextView) findViewById(R.id.textjarak);
        jaraklokasi();
        UploadImageOnServerButtonmasuk = (Button) findViewById(R.id.buttonUploadabsenMasuk);
        UploadImageOnServerButtonpulang = (Button) findViewById(R.id.buttonUploadabsenPulang);
        ShowSelectedImage = (ImageView) findViewById(R.id.imageView3profile);
        if (ContextCompat.checkSelfPermission(MulaiAbsen.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                        5);
            }
        }
        byteArrayOutputStream = new ByteArrayOutputStream();
//        UploadImageOnServerButton.setEnabled(true);
        UploadImageOnServerButtonmasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sidjadwal =id_jadwal.getText().toString();
                sket =txtketabsensi.getText().toString();
                if (FixBitmap==null){
                    Toasty.normal(MulaiAbsen.this, "Gambar Kosong!", Toasty.LENGTH_SHORT).show();
                }else if(sidjadwal.equals("")) {
                    Toasty.normal(MulaiAbsen.this, "Andat tidak bisa absen!", Toasty.LENGTH_SHORT).show();
                }else if(distance>=2) {
                    Toasty.normal(MulaiAbsen.this, "Jarak Lokasi Tidak dikampus!", Toasty.LENGTH_SHORT).show();
                }else {
                    UploadImageToServerMasuk();
                    FixBitmap=null;
                    ShowSelectedImage.setImageBitmap(null);
                    id_jadwal.setText("");
                    showMyLocation();
                    callJam();
                    callButtonAktif();
                }


            }
        });
        UploadImageOnServerButtonpulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sidjadwal =id_jadwal.getText().toString();
                if (FixBitmap==null){
                    Toasty.normal(MulaiAbsen.this, "Gambar Kosong!", Toasty.LENGTH_SHORT).show();
                }else if(sidjadwal.equals("")) {
                    Toasty.normal(MulaiAbsen.this, "Andat tidak bisa absen!", Toasty.LENGTH_SHORT).show();
                }else if(distance>=15000) {
                    Toasty.normal(MulaiAbsen.this, "Jarak Lokasi Tidak dikampus!", Toasty.LENGTH_SHORT).show();
                }else {
                    UploadImageToServerPulang();
                    FixBitmap=null;
                    ShowSelectedImage.setImageBitmap(null);
                    id_jadwal.setText("");
                    showMyLocation();
                    callJam();
                    callButtonAktif();
                }


            }
        });
    }
    private void jaraklokasi() {
        final int R = 6371; // Radious of the earth
        Double latDistance = toRad(latitudekampus-latitudesaatini);
        Double lonDistance = toRad(longitudekampus-longitudesaatini);
        Double a = Math.sin(latDistance / 15000) * Math.sin(latDistance / 15000) +
                Math.cos(toRad(latitudesaatini)) * Math.cos(toRad(latitudekampus)) *
                        Math.sin(lonDistance / 15000) * Math.sin(lonDistance / 15000);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        distance = R * c;
        jarak.setText("Jarak Kampus :" + distance +" Km");
    }

    private static Double toRad(Double value) {
        return value * Math.PI / 180;
    }

    private void onMyMapReady(GoogleMap googleMap) {
        myMap = googleMap;
        myMap.addCircle(new CircleOptions()
                .center(new LatLng(latitudekampus, longitudekampus))
                .radius(600)
                .strokeColor(Color.RED)
                .fillColor(Color.BLUE));
        myMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                myProgress.dismiss();
                askPermissionsAndShowMyLocation();
            }
        });
        myMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        myMap.getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        myMap.setMyLocationEnabled(true);
    }
    private void askPermissionsAndShowMyLocation() {
        if (Build.VERSION.SDK_INT >= 23) {
            int accessCoarsePermission
                    = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int accessFinePermission
                    = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (accessCoarsePermission != PackageManager.PERMISSION_GRANTED
                    || accessFinePermission != PackageManager.PERMISSION_GRANTED) {
                String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION};
                ActivityCompat.requestPermissions(this, permissions,
                        REQUEST_ID_ACCESS_COURSE_FINE_LOCATION);
                return;
            }
        }
        this.showMyLocation();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_ID_ACCESS_COURSE_FINE_LOCATION: {
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission granted!", Toast.LENGTH_LONG).show();
                    this.showMyLocation();
                }
                else {
                    Toast.makeText(this, "Permission denied!", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }
    private String getEnabledLocationProvider() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
        boolean enabled = locationManager.isProviderEnabled(bestProvider);
        if (!enabled) {
            Toast.makeText(this, "No location provider enabled!", Toast.LENGTH_LONG).show();
            Log.i(MYTAG, "No location provider enabled!");
            return null;
        }
        return bestProvider;
    }
    private void showMyLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        String locationProvider = this.getEnabledLocationProvider();
        if (locationProvider == null) {
            return;
        }
        // Millisecond
        final long MIN_TIME_BW_UPDATES = 1000;
        // Met
        final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;
        Location myLocation = null;
        try {
            locationManager.requestLocationUpdates(
                    locationProvider,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);
            myLocation = locationManager
                    .getLastKnownLocation(locationProvider);
        }
        catch (SecurityException e) {
            Toast.makeText(this, "Show My Location Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            Log.e(MYTAG, "Show My Location Error:" + e.getMessage());
            e.printStackTrace();
            return;
        }
        if (myLocation != null) {
            LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)             // Sets the center of the map to location user
                    .zoom(15)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            myMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            MarkerOptions option = new MarkerOptions();
            option.title("Lokasi Anda");
            option.snippet("....");
            option.position(latLng);
            Marker currentMarker = myMap.addMarker(option);
            currentMarker.showInfoWindow();

            latitudesaatini= Double.parseDouble(String.valueOf(myLocation.getLatitude()));
            longitudesaatini= Double.parseDouble(String.valueOf(myLocation.getLongitude()));

            System.out.println("Jarak Antara latitude :" + String.valueOf(myLocation.getLatitude())+" Logitude :"+String.valueOf(myLocation.getLongitude()));
            latitude.setText(String.valueOf(myLocation.getLatitude()));
            longitude.setText(String.valueOf(myLocation.getLongitude()));
            jaraklokasi();
        } else {
            Toast.makeText(this, "Location Tidak deteksi!", Toast.LENGTH_LONG).show();
            Log.i(MYTAG, "Location not found");
        }

    }
    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void ambillokasi() {
        myProgress = new ProgressDialog(this);
        myProgress.setTitle("Map Loading ...");
        myProgress.setMessage("Please wait...");
        myProgress.setCancelable(true);
        myProgress.show();
        SupportMapFragment mapFragment
                = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                onMyMapReady(googleMap);
            }
        });
    }

    //
    public void UploadImageToServerMasuk() {
        if (FixBitmap!=null) {
            FixBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);

            byteArray = byteArrayOutputStream.toByteArray();

            ConvertImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
        }else {
            ConvertImage="";
        }
        class AsyncTaskUploadClass extends AsyncTask<Void, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(MulaiAbsen.this, "Image is Uploading", "Please Wait", false, false);
            }

            @Override
            protected void onPostExecute(String string1) {
                super.onPostExecute(string1);
                progressDialog.dismiss();
                Toasty.info(MulaiAbsen.this, string1, Toasty.LENGTH_LONG).show();
                callJam();
                callButtonAktif();


            }

            @Override
            protected String doInBackground(Void... params) {
                ImageProcessClassMasuk imageProcessClass = new ImageProcessClassMasuk();
                HashMap<String, String> HashMapParams = new HashMap<String, String>();
                HashMapParams.put("nim", cnim);
                HashMapParams.put("id_jadwal", sidjadwal.toString());
                HashMapParams.put("ket", sket.toString());
                HashMapParams.put(ImageName, ConvertImage);
                String FinalData = imageProcessClass.ImageHttpRequest(URL_SERVER.TAMBAHABSENSIMASUK, HashMapParams);
                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();
        AsyncTaskUploadClassOBJ.execute();
    }
    public void UploadImageToServerPulang() {
        if (FixBitmap!=null) {
            FixBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);

            byteArray = byteArrayOutputStream.toByteArray();

            ConvertImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
        }else {
            ConvertImage="";
        }
        class AsyncTaskUploadClass extends AsyncTask<Void, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(MulaiAbsen.this, "Image is Uploading", "Please Wait", false, false);
            }

            @Override
            protected void onPostExecute(String string1) {
                super.onPostExecute(string1);
                progressDialog.dismiss();
                Toasty.info(MulaiAbsen.this, string1, Toasty.LENGTH_LONG).show();
                callJam();
                callButtonAktif();


            }

            @Override
            protected String doInBackground(Void... params) {
                ImageProcessClassPulang imageProcessClass = new ImageProcessClassPulang();
                HashMap<String, String> HashMapParams = new HashMap<String, String>();
                HashMapParams.put("nim", cnim);
                HashMapParams.put("id_jadwal", sidjadwal.toString());
                HashMapParams.put(ImageName, ConvertImage);
                String FinalData = imageProcessClass.ImageHttpRequest(URL_SERVER.TAMBAHABSENSIPULANG, HashMapParams);
                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();
        AsyncTaskUploadClassOBJ.execute();
    }

    @Override
    public void onRefresh() {
        callJam();
        callButtonAktif();
    }

    public class ImageProcessClassMasuk {
        public String ImageHttpRequest(String requestURL, HashMap<String, String> PData) {
            StringBuilder stringBuilder = new StringBuilder();
            try {
                url = new URL(requestURL);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setReadTimeout(20000);
                httpURLConnection.setConnectTimeout(20000);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                outputStream = httpURLConnection.getOutputStream();
                bufferedWriter = new BufferedWriter(
                        new OutputStreamWriter(outputStream, "UTF-8"));
                bufferedWriter.write(bufferedWriterDataFN(PData));
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                RC = httpURLConnection.getResponseCode();
                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    stringBuilder = new StringBuilder();
                    String RC2;
                    while ((RC2 = bufferedReader.readLine()) != null) {
                        stringBuilder.append(RC2);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {
            stringBuilder = new StringBuilder();
            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {
                if (check)
                    check = false;
                else
                    stringBuilder.append("&");
                stringBuilder.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));
                stringBuilder.append("=");
                stringBuilder.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }
            return stringBuilder.toString();
        }
    }
    public class ImageProcessClassPulang {
        public String ImageHttpRequest(String requestURL, HashMap<String, String> PData) {
            StringBuilder stringBuilder = new StringBuilder();
            try {
                url = new URL(requestURL);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setReadTimeout(20000);
                httpURLConnection.setConnectTimeout(20000);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                outputStream = httpURLConnection.getOutputStream();
                bufferedWriter = new BufferedWriter(
                        new OutputStreamWriter(outputStream, "UTF-8"));
                bufferedWriter.write(bufferedWriterDataFN(PData));
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                RC = httpURLConnection.getResponseCode();
                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    stringBuilder = new StringBuilder();
                    String RC2;
                    while ((RC2 = bufferedReader.readLine()) != null) {
                        stringBuilder.append(RC2);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {
            stringBuilder = new StringBuilder();
            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {
                if (check)
                    check = false;
                else
                    stringBuilder.append("&");
                stringBuilder.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));
                stringBuilder.append("=");
                stringBuilder.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }
            return stringBuilder.toString();
        }
    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Photo Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    FixBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    ShowSelectedImage.setImageBitmap(FixBitmap);
                    UploadImageOnServerButtonmasuk.setVisibility(View.VISIBLE);
                    UploadImageOnServerButtonpulang.setVisibility(View.VISIBLE);


                } catch (IOException e) {
                    e.printStackTrace();
                    Toasty.error(MulaiAbsen.this, "Failed!", Toasty.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            FixBitmap = (Bitmap) data.getExtras().get("data");
            ShowSelectedImage.setImageBitmap(FixBitmap);
            UploadImageOnServerButtonmasuk.setVisibility(View.VISIBLE);
            UploadImageOnServerButtonpulang.setVisibility(View.VISIBLE);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menucamera, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuambilcamera_absen) {
            showPictureDialog();
        }
        if (id == R.id.refreshlokasi) {
            ambillokasi();

        }
        return super.onOptionsItemSelected(item);
    }

    private void bacaPreferensi() {
        SharedPreferences pref = getSharedPreferences("akun", MODE_PRIVATE);
        cnim = pref.getString("nim", "0");
        cnama_mahasiswa = pref.getString("nama_mahasiswa", "0");
        cid_semester = pref.getString("id_semester", "0");
        cnama_semester = pref.getString("nama_semester", "0");
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(MulaiAbsen.this,MenuUtama.class));
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
    private void callJam() {
        swipe.setRefreshing(true);
        // Creating volley request obj
        StringRequest jArr = new StringRequest(Request.Method.POST, URL_SERVER.DATAJAM, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);

                    int value = jObj.getInt(TAG_VALUE);

                    if (value == 1) {

                        String getObject = jObj.getString(TAG_RESULTS);
                        JSONArray jsonArray = new JSONArray(getObject);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);

                            String sid_jadwal = obj.getString("id_jadwal");
                            String snama_matkul = obj.getString("nama_matkul");
                            String snama_dosen = obj.getString("nama_dosen");
                            String snama_ruang = obj.getString("nama_ruang");
                            String shari = obj.getString("hari");
                            String sjamawal = obj.getString("jam_awal");
                            String sjamakhir = obj.getString("jam_akhir");
                            String sket = obj.getString("ket");
                            id_jadwal.setText(sid_jadwal);
                            viewjadwal.setText("MataPelajaran: "+snama_matkul+". Guru: "+snama_dosen+". Ruang: "+snama_ruang+". Hari: "+shari+". Jam Awal: "+sjamawal+". Jam Akhir: "+sjamakhir+". Ket: "+sket);
                            txtketabsensi.setText(sket);
                        }

                    } else {
                        id_jadwal.setText("");
                        viewjadwal.setText(jObj.getString(TAG_MESSAGE));
                        Toast.makeText(getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi Error", Toast.LENGTH_SHORT).show();
                swipe.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("nim", cnim);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jArr, tag_json_obj);
    }
    private void callButtonAktif() {
        swipe.setRefreshing(true);
        // Creating volley request obj
        StringRequest jArr = new StringRequest(Request.Method.POST, URL_SERVER.CEKBUTTONAKTIF, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);

                    int value = jObj.getInt(TAG_VALUE);

                    if (value == 1) {

                        String getObject = jObj.getString(TAG_RESULTS);
                        JSONArray jsonArray = new JSONArray(getObject);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            String buttonaktif = obj.getString("buttonaktif");
                            if (buttonaktif.equals("Masuk")){
                                UploadImageOnServerButtonmasuk.setEnabled(true);
                                UploadImageOnServerButtonpulang.setEnabled(false);
                            }else {
                                UploadImageOnServerButtonpulang.setEnabled(true);
                                UploadImageOnServerButtonmasuk.setEnabled(false);
                            }
                        }

                    } else {
                        id_jadwal.setText("");
                        viewjadwal.setText(jObj.getString(TAG_MESSAGE));
                        Toast.makeText(getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi Error", Toast.LENGTH_SHORT).show();
                swipe.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("nim", cnim);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jArr, tag_json_obj);
    }


}



