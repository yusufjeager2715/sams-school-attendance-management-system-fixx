package com.example.hp.absensi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.example.hp.absensi.adapter.AdapterHistori;
import com.example.hp.absensi.app.AppController;
import com.example.hp.absensi.model.DataModelHistori;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataHistori extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    ProgressDialog pDialog;
    List<DataModelHistori> listData = new ArrayList<DataModelHistori>();
    AdapterHistori adapter;
    SwipeRefreshLayout swipe;
    ListView list_view;
    String _id;
    String cnim, cnama_mahasiswa, ckelas;
    public static final String url_data = URL_SERVER.DATAHISTORI;
    String imgurl = URL_SERVER.GAMBARHISTORI;

    private static final String TAG = DataHistori.class.getSimpleName();

    public static final String TAG_IDABSENSI = "id_absensi";
    public static final String TAG_IDJAWAL = "id_jadwal";
    public static final String TAG_WAKTU = "waktu";
    public static final String TAG_STATUS = "status";
    public static final String TAG_KET = "ket";
    public static final String TAG_FILE= "file";

    public static final String TAG_RESULTS = "results";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_VALUE = "value";

    String tag_json_obj = "json_obj_req";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datahistori);
        bacaPreferensi();

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refreshikm);
        list_view = (ListView) findViewById(R.id.list_viewikm);

        adapter = new AdapterHistori(DataHistori.this, listData);
        list_view.setAdapter(adapter);


        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           callData();
                       }
                   }
        );

    }
    private void callData() {
        listData.clear();
        adapter.notifyDataSetChanged();
        swipe.setRefreshing(true);

        // Creating volley request obj
        StringRequest jArr = new StringRequest(Request.Method.POST, url_data, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);

                    int value = jObj.getInt(TAG_VALUE);

                    if (value == 1) {
                        listData.clear();
                        adapter.notifyDataSetChanged();

                        String getObject = jObj.getString(TAG_RESULTS);
                        JSONArray jsonArray = new JSONArray(getObject);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);

                            DataModelHistori item = new DataModelHistori();

                            item.setId_absen(obj.getString(TAG_IDABSENSI));
                            item.setId_jadwal(obj.getString(TAG_IDJAWAL));
                            item.setWaktu(obj.getString(TAG_WAKTU));
                            item.setStatus(obj.getString(TAG_STATUS));
                            item.setKet(obj.getString(TAG_KET));
                            item.setFile(imgurl+obj.getString(TAG_FILE));
                            listData.add(item);
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

                adapter.notifyDataSetChanged();
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                swipe.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("nim", cnim);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jArr, tag_json_obj);
    }



    @Override
    public void onRefresh() {
        callData();
    }

    private void bacaPreferensi() {
        SharedPreferences pref = getSharedPreferences("akun", MODE_PRIVATE);
        cnim = pref.getString("nim", "0");
        cnama_mahasiswa = pref.getString("nama_mahasiswa", "0");
        ckelas = pref.getString("kelas", "0");
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(DataHistori.this,MenuUtama.class));
        finish();
    }

}