package com.example.hp.absensi.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp.absensi.R;
import com.example.hp.absensi.model.DataModelHistori;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by KUNCORO on 09/08/2017.
 */

public class AdapterHistori extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataModelHistori> item;

    public AdapterHistori(Activity activity, List<DataModelHistori> item) {
        this.activity = activity;
        this.item = item;
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public Object getItem(int location) {
        return item.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.actiivity_listhistori, null);

        TextView txtid_absensi = (TextView) convertView.findViewById(R.id.txtid_absensi_histori);
        TextView txt_idjaddwalabsen = (TextView) convertView.findViewById(R.id.txt_idjaddwalabsen_histori);
        TextView txt_waktuabsen_histori = (TextView) convertView.findViewById(R.id.txt_waktuabsen_histori);
        TextView txt_statusabsen_histori = (TextView) convertView.findViewById(R.id.txt_statusabsen_histori2);
        TextView txt_ketabsen_histori = (TextView) convertView.findViewById(R.id.txt_ketabsen_histori3);
        ImageView gambar = (ImageView) convertView.findViewById(R.id.rowimageButtonpenganan);


        txtid_absensi.setText(item.get(position).getId_absen());
        txt_idjaddwalabsen.setText(item.get(position).getId_jadwal());
        txt_waktuabsen_histori.setText(item.get(position).getWaktu());
        txt_statusabsen_histori.setText(item.get(position).getStatus());
        txt_ketabsen_histori.setText(item.get(position).getKet());
        Picasso.with(activity).load(item.get(position).getFile())
                .into(gambar);


        return convertView;
    }
}